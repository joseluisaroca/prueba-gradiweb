<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class AppointmentsController extends Controller{
    public $database;
    public $reference;

    public function __construct(){
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path().'/prueba-gradiweb-jla-firebase-adminsdk-5xplc-1e79a58921.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://prueba-gradiweb-jla.firebaseio.com')
        ->create();

        $this->database = $firebase->getDatabase();
        $this->reference = $this->database->getReference('servicios');
    }

    public function crearReserva($idServicio, Request $request){
        $db =  $this->database;
        $searchResult =  $this->database->getReference('servicios/'.$idServicio)->getValue();

        try {
            if(!$searchResult){
                throw new \Exception("El servicio no existe", 404);
            } else {

                if(!$request->has('fecha')){
                    throw new \Exception("Debe enviarse una fecha válida (YYYY-MM-DD)", 403);
                } else {
                    if(!$request->has('nombre') || !$request->has('documento')){
                        throw new \Exception("Debe enviarse el nombre y el documento de la persona que tomará la reserva", 403);
                    }

                    $fecha = $request->get('fecha');
                    $nombre = $request->get('nombre');
                    $documento = $request->get('documento');
                    if(strtotime(date('Y-m-d')) > strtotime($fecha)){
                        throw new \Exception("Debe enviarse una fecha válida igual o mayor a hoy (YYYY-MM-DD)", 403);
                    } else {
                        $diaSemana =  $this->saber_dia($fecha);     
                        $diasTrabajo = $searchResult['dias_trabajo'];  
                        
                        if(in_array($diaSemana, $diasTrabajo)){
                            $reservas = (isset($searchResult['reservas']) ?  $searchResult['reservas']: []);

                            if(isset($reservas[$fecha])){
                                throw new \Exception("Ya existe una reserva para éste día", 403);
                            } else {
                                $uuid = $this->reference->push()->getKey();

                                $reservas[$fecha] = [
                                    'nombre' => $nombre,
                                    'documento' => $documento,
                                    'id' => $uuid
                                ];

                                $searchResult['reservas'] = $reservas;

                                $updates = [
                                    $idServicio => $searchResult
                                ];
                
                                $this->reference->update($updates);
                                $searchResult =  $this->database->getReference('servicios/'.$idServicio)->getValue();
                
                                if($searchResult){
                                    if(isset($searchResult['reservas']) && isset($searchResult['reservas'][$fecha])){
                                        return response()->json([
                                            'mensaje' => "La reserva fue creada con el identificador ".$uuid
                                        ], 200);
                                    }
                                } else {
                                    throw new \Exception("Se generó un error en la actualización", 500);
                                }
                            }
                        } else {
                            throw new \Exception("Éste servicio no puede ser asignado para éste día", 403);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    public function getReservas(Request $request){
        $allServices =  $this->reference->getValue();
        $allAppoint = [];

        try {
            if(!$request->has('fecha_desde') || !$request->has('fecha_hasta')){
                throw new \Exception("Debe enviarse un rango de fechas válidas (YYYY-MM-DD)", 403);
            } else {
                $fecha_desde = $request->get('fecha_desde');
                $fecha_hasta = $request->get('fecha_hasta');
                if(strtotime($fecha_desde) > strtotime($fecha_hasta)){
                    throw new \Exception("La fecha de inicio debe ser anterior a la fecha final", 403);
                } else {
                    foreach ($allServices as $servicio) {
                        $reservas = (isset($servicio['reservas']) ? $servicio['reservas']: []);
                        foreach ($reservas as $key => $reserva) {
                            $fecha_llave = strtotime($key);

                            if(strtotime($fecha_desde) < $fecha_llave && $fecha_llave < strtotime($fecha_hasta)){
                                $reserva['servicio'] = $servicio['nombre'];
                                if(!isset($allAppoint[$key])){
                                    $allAppoint[$key] = [];
                                }
                                $push =  array_push($allAppoint[$key], $reserva);
                            }
                        }
                    }

                    if(count($allAppoint) > 0){
                        return response()->json([
                            'mensaje' => "Reservas traidas correctamente",
                            'data' => $allAppoint
                        ], 200);
                    } else {
                        throw new \Exception("No se encontraron reservas en la fecha", 404);
                    }
                }
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    function saber_dia($fecha) {
        $dias = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
        $nombredia = $dias[date('N', strtotime($fecha))];
        return $nombredia;
    }    
}