<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class ServicesController extends Controller{
    public $database;
    public $reference;

    public function __construct(){
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path().'/prueba-gradiweb-jla-firebase-adminsdk-5xplc-1e79a58921.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://prueba-gradiweb-jla.firebaseio.com')
        ->create();

        $this->database = $firebase->getDatabase();
        $this->reference = $this->database->getReference('servicios');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $database =  $this->database;
        $reference = $this->reference;

        $valores = $reference->getValue();

        return response()->json([
            'mensaje' => "Datos mostrados exitosamente",
            'data' => $valores
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $db =  $this->database;
        $newServiceKey = $this->createSlug($request->get('nombre'));
        
        try {
            $searchKey =  $this->database->getReference('servicios/'.$newServiceKey)->getValue();

            if($searchKey){
                throw new \Exception("El servicio ya se encuentra creado", 403);
            } else {
                $datos = $request->all();

                $datos['dias_trabajo'] = explode(',', $datos['dias_trabajo']);

                $new_array = [
                    $newServiceKey => $datos
                ];

                $this->reference->update($new_array);
                $searchResult =  $this->database->getReference('servicios/'.$newServiceKey)->getValue();

                if($searchResult){
                    return response()->json([
                        'mensaje' => "El servicio fue creado con éxito con el id ".$newServiceKey
                    ], 200);
                } else {
                    throw new \Exception("Se generó un error en la creación", 500);
                }
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $db =  $this->database;
        $searchResult =  $this->database->getReference('servicios/'.$id)->getValue();

        try {
            if(!$searchResult){
                throw new \Exception("El servicio no existe", 404);
            } else {
                return response()->json([
                    'mensaje' => "Datos mostrados exitosamente",
                    'data' => $searchResult
                ], 200);
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $db =  $this->database;
        $searchResult =  $this->database->getReference('servicios/'.$id)->getValue();

        try {
            if(!$searchResult){
                throw new \Exception("El servicio no existe", 404);
            } else {
                $datos = $request->all();
                $datos['dias_trabajo'] = explode(',', $datos['dias_trabajo']);

                $updates = [
                    $id => $datos
                ];

                $this->reference->update($updates);
                $searchResult =  $this->database->getReference('servicios/'.$id)->getValue();

                if($searchResult){
                    return response()->json([
                        'mensaje' => "El servicio fue actualizado con éxito"
                    ], 200);
                } else {
                    throw new \Exception("Se generó un error en la actualización", 500);
                }
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $db =  $this->database;
        $searchResult =  $this->database->getReference('servicios/'.$id)->getValue();

        try {
            if(!$searchResult){
                throw new \Exception("El servicio no existe", 404);
            } else {
                $delete = $this->database->getReference('servicios/'.$id)->remove();

                $searchResult =  $this->database->getReference('servicios/'.$id)->getValue();
                if(!$searchResult){
                    return response()->json([
                        'mensaje' => "Datos borrados exitosamente"
                    ], 200);
                } else {
                    throw new \Exception("El servicio no fue borrado", 404);
                }
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    public function listadodia($diasemana){
        try {
            if($diasemana > 7 || $diasemana < 1){
                throw new \Exception("Se debe seleccionar un día entre el Lunes (1) y Domingo (7)", 403);
            } else {
                $dias = array('Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');
                $diabusqueda = $dias[$diasemana-1];
                $arrServicios = [];

                $servicios = $this->reference->getValue();

                foreach ($servicios as $key => $servicio) {
                    if(in_array($diabusqueda, $servicio['dias_trabajo'])){
                        $arrServicios[$key] = $servicio;
                    }
                }
                
                if(count($arrServicios) > 0){
                    return response()->json([
                        'mensaje' => "Servicios traidos correctamente",
                        'data' => $arrServicios
                    ], 200);
                } else {
                    throw new \Exception("No existen servicios para el día asignado", 404);
                }
            }
        } catch (\Exception $e) {
            $error_code = $e->getCode();
            if($error_code == "0"){
                $error_code = 403;
            }

            return response()->json([
                'mensaje' => $e->getMessage()
            ], $error_code);
        }
    }

    private function createSlug($str, $max=30){
        $out = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $out = substr(preg_replace("/[^-\/+|\w ]/", '', $out), 0, $max);
        $out = strtolower(trim($out, '-'));
        $out = preg_replace("/[\/_| -]+/", '-', $out);

        return $out;
    }

}
