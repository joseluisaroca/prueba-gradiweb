<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('servicios', 'ServicesController');
Route::post('servicios/{diasemana}', 'ServicesController@listadodia')->name('listadodia');
Route::post('reservas/{idServicio}', 'AppointmentsController@crearReserva')->name('crearReserva');
Route::post('reservas', 'AppointmentsController@getReservas')->name('getReservas');



